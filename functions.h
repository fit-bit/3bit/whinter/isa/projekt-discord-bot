// functions.h
// Autor: Matěj Kudera (xkuder04)
// Pomocné funkce pro bota

#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>
#include <string>
#include <regex>
#include <sstream>
#include <algorithm>
#include <map>
#include <list>

#include <sys/types.h>
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

int check_args(bool *verbose, std::string *bot_access_token, int argc, char *argv[]);
int create_ssl_connection(int *sock_fd, SSL **ssl_connection, SSL_CTX **ssl_certificate);
void close_ssl_connection(int *sock_fd, SSL **ssl_connection, SSL_CTX **ssl_certificate);
std::string remove_message_header(std::string message);
int stop_message_read(std::string message, const char last_read_buffer[1024]);
int get_channel_information(std::string message, std::string *isa_channel_id, std::string *last_message_id);
std::list <std::string> devide_messages(std::string messages_string, std::string delimiter);
int get_message_information(std::string message, std::string *content, std::string *username, std::string *message_id);
int get_json_reply(SSL *ssl_connection, std::string *json_string, std::string message_to_send);

#endif
// END functions.h