// isabot.h
// Autor: Matěj Kudera (xkuder04)
// Deklarace pro program discord bota

#ifndef ISABOT_H
#define ISABOT_H

// Chybové kódy
#define ALL_OK 0
#define UNKNOWN_PARAM 1
#define BAD_PARAM_FORMAT 2
#define SSL_CERTIFICAT_ERROR 3
#define IP_RESOLUTION_ERROR 4
#define SOCKET_CREATION_ERROR 5
#define SERVER_CONNECTION_ERROR 6
#define SSL_CONNECTION_ERROR 7
#define SSL_SEND_ERROR 8

#define UNEXPECTED_SERVER_REPLY 9
#define BOT_NOT_IN_GUILD 10
#define CHANNEL_DOES_NOT_EXIST 11

#endif
// END isabot.h