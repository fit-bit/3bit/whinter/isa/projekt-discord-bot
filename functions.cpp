// functions.cpp
// Autor: Matěj Kudera (xkuder04)
// Pomocné funkce pro bota

#include <iostream>
#include <string>
#include <regex>
#include <sstream>
#include <algorithm>
#include <map>
#include <list>

#include <sys/types.h>
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "functions.h"
#include "isabot.h"

// Kontrola argumentů programu
// Vrací 3 když vše v pořádku, jinak je vrácen kód pro ukončení programu
int check_args(bool *verbose, std::string *bot_access_token, int argc, char *argv[])
{
    // TODO 2x stejný parametr
    // TODO co udělat když špatný param
    // TODO vždy -- před celou variantou?
    if (argc == 1)
    {
        return ALL_OK;
    }
    else
    {
        int i = 1;
        while (i < argc)
        {
            if ((std::string(argv[i]) == "-h") || (std::string(argv[i]) == "--help"))
            {
                return ALL_OK;
            }
            else if ((std::string(argv[i]) == "-v") || (std::string(argv[i]) == "--verbose"))
            {
                *verbose = true;
            }
            else if (std::string(argv[i]) == "-t")
            {
                // načtení tokenu bota
                if (i+1 < argc)
                {
                    i++;
                    *bot_access_token = std::string(argv[i]);
                }
                else
                {
                    return BAD_PARAM_FORMAT;
                }
            }
            else
            {
                // Neznámy parametr -> chyba
                return UNKNOWN_PARAM;
            }
            
            i++;
        }
    }

    return 3;
}

// Vytvoření ssl spojení se serverem
// Vrací 0 když vše v pořádku, jinak je vrácen chybový kód
int create_ssl_connection(int *sock_fd, SSL **ssl_connection, SSL_CTX **ssl_certificate)
{
    std::string discord_server_url = "discord.com";
    std::string port = "443";

    struct hostent *host;
    struct sockaddr_in addr;

    // Inicializace ssl
    SSL_library_init();
    OpenSSL_add_all_algorithms(); 
    SSL_load_error_strings();

    // Vytvoření ssl cetrifikátu
    if ( (*ssl_certificate = SSL_CTX_new(SSLv23_client_method())) == NULL ) 
    {
        return SSL_CERTIFICAT_ERROR;
    }

    // Získání IP adresy discordu
    if ( (host = gethostbyname(discord_server_url.c_str())) == NULL )
    {
        SSL_CTX_free(*ssl_certificate); 

        return IP_RESOLUTION_ERROR;
    }

    // Vytvoření soketu
    if((*sock_fd = socket(PF_INET, SOCK_STREAM, 0)) < 0)
    {
        SSL_CTX_free(*ssl_certificate); 

        return SOCKET_CREATION_ERROR;
    }

    // Navázání spojení se serverem na konkrétním portu
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(port.c_str()));
    addr.sin_addr.s_addr = *(long*)(host->h_addr);
    if ( connect(*sock_fd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
    {
        SSL_CTX_free(*ssl_certificate); 

        return SERVER_CONNECTION_ERROR;
    }
    
    // Navázání ssl spojení na soketu
    *ssl_connection = SSL_new(*ssl_certificate);      
    SSL_set_fd(*ssl_connection, *sock_fd);    
    if ( SSL_connect(*ssl_connection) == 0 ) 
    {
        SSL_CTX_free(*ssl_certificate); 
        close(*sock_fd);

        return SSL_CONNECTION_ERROR;
    }

    return 0;

}

// Uzavření ssl spojení se serverem
void close_ssl_connection(int *sock_fd, SSL **ssl_connection, SSL_CTX **ssl_certificate)
{
    SSL_free(*ssl_connection);
    close(*sock_fd);
    SSL_CTX_free(*ssl_certificate);
}

// Odtranění hlavičky z odpovědi serveru
std::string remove_message_header(std::string message)
{
    std::istringstream message_stream(message);
    std::string line;
    std::string content;
    bool got_empty_line = false;

    while (getline(message_stream, line))
    {
        if (got_empty_line == true)
        {
            content.append(line);
        }

        if (line.length() == 1)
        {
            got_empty_line = true;
        }
    }

    return content;
}

// Určení konce čtení zprávy z SLL soketu
// Vrací 0 když konec, 1 když se má pokračovat ve štení
int stop_message_read(std::string message, const char last_read_buffer[1024])
{
    // Konec čtení -> když je přenos typu chunked tak je čtení ukončeno nulou, jinak je v hlavičce content-lenght
    std::smatch header_found;
    if(regex_search(message, header_found, std::regex("Content-Length: [0-9]*")))
    {
        // Získání obsahu a určení podle content lenght jestli je načteno vše

        // délka obsahu
        unsigned lenght = std::stoi(header_found.str().erase(0,16));

        std::string content = remove_message_header(message);
            
        // Pokud sedí velikost v content-lenght a velikost načtených dat -> konec čtení
        if (content.length() == lenght)
        {
            return 0;
        }
    }
    else
    {
        // Chunked přenos ukončen nulou
        if ((last_read_buffer[0]) == '0')
        {
            return 0;
        }
    }

    return 1;
}

// Získání informací o kanálu isa-bot
// Vrací 0 pokud se podařilo najít informace, jinak 1
int get_channel_information(std::string message, std::string *isa_channel_id, std::string *last_message_id)
{
    std::string channel_id;
    std::string message_id;
    std::smatch channel_found;

    if(regex_search(message, channel_found, std::regex("\"id\": \"[0-9]*\", \"last_message_id\": (\"[0-9]*\"|null), \"type\": 0, \"name\": \"isa-bot\"")))
    {
        // Příklad najitého textu "id": "760833970627280946", "last_message_id": "762302526744690709", "type": 0, "name": "isa-bot"
        std::string channel_informations = channel_found.str();

        // Vystříhání id kanálu z textu
        channel_id = channel_informations.substr(0, channel_informations.find(","));
        channel_id.erase(0, 7);
        channel_id.erase(channel_id.length() - 1, channel_id.length());

        // odstranění channel id a , a mezera z textu
        channel_informations.erase(0, channel_informations.find(",") + 2); 

        // Vystříhání id poslední zprávy z textu
        // null pokud žádná poslední zpráva není
        message_id = channel_informations.substr(0, channel_informations.find(","));;
        message_id.erase(0, 19);

        if (message_id != "null")
        {
            message_id.erase(0, 1);
            message_id.erase(message_id.length() - 1, message_id.length());
        } 
    }
    else
    {
        return 1;
    }
    
    (*isa_channel_id) = channel_id;
    (*last_message_id) = message_id;
    return 0;
}
 
// Rozdělení zpráv podle oddělovače
// Vrací pole ve kterém jsou jednotlivé zprávy
std::list <std::string> devide_messages(std::string messages_string, std::string delimiter)
{
    size_t pos = 0;
    std::list <std::string> messages;

    // Kontrola špatného formátu oddělovače
    if ((delimiter.empty()) || (delimiter.length() >= messages_string.length()))
    {
        return messages;
    }

    // Rozdělení zpráv
    while ((pos = messages_string.find(delimiter)) != messages_string.npos) 
    {
        std::string message = messages_string.substr(0, pos);
        messages.push_back(message);
        messages_string.erase(0, pos + delimiter.length());

    }

    // Přidání poslední zprávy který zbyde v řetězci
    if (!messages_string.empty())
    {
        messages.push_back(messages_string);
    }
    
    return messages;
}

// Záskání informací ze správy
// Funkce vrací 0 pokud se vše povede najít jinak 1
int get_message_information(std::string message, std::string *content, std::string *username, std::string *message_id)
{
    std::string content_str;
    std::string username_str;
    std::string message_id_str;
    std::smatch item_found;

    if(regex_search(message, item_found, std::regex("\"content\": \".*\"")))
    {
        content_str = item_found.str();
        content_str.erase(content_str.find(","), content_str.length());
        content_str.erase(0, 12);
        content_str.erase(content_str.length() - 1, content_str.length());
    }
    else
    {
        return 1;
    }

    if(regex_search(message, item_found, std::regex("\"username\": \".*\"")))
    {
        username_str = item_found.str();
        username_str.erase(username_str.find(","), username_str.length());
        username_str.erase(0, 13);
        username_str.erase(username_str.length() - 1, username_str.length());
    }
    else
    {
        return 1;
    }

    if(regex_search(message, item_found, std::regex("\"id\": \"[0-9]*\"")))
    {
        message_id_str = item_found.str();
        message_id_str.erase(0, 7);
        message_id_str.erase(message_id_str.length() - 1, message_id_str.length());
    }
    else
    {
        return 1;
    }


    (*content) = content_str;
    (*username) = username_str;
    (*message_id) = message_id_str;
    return 0;
}


// Získání json odpovědi na dotaz od serveru
// Vrací 0 když vše v pořádku, jinak je vrácen chybový kód
int get_json_reply(SSL *ssl_connection, std::string *json_string, std::string message_to_send)
{
    std::string str_buffer;
    char buffer[1024] = {0};
    int readed_bytes;

    // Vyčištění výstupního stringu
    (*json_string).clear();

    if (SSL_write(ssl_connection, message_to_send.c_str(), message_to_send.length()) < 0) /* encrypt & send message */
    {
        return SSL_SEND_ERROR;
    }

    // Načtení včech dat z http odpovědi
    while ((readed_bytes = SSL_read(ssl_connection, buffer, 1024)))
    {
        // Vložení ukončovače řetězce
        if (readed_bytes != 1024)
        {
            buffer[readed_bytes] = 0;
        }
        
        str_buffer.append(buffer);

        if (stop_message_read(str_buffer, buffer) == 0)
        {
            break;
        }
    } 

    if (str_buffer.empty())
    {
        return UNEXPECTED_SERVER_REPLY;
    }

    // Odstranění http hlavičky z odpovědi
    (*json_string) = remove_message_header(str_buffer);

    // Zajištění čekacího času do poslání dalšího requestu aby nás server neodstřihl
    sleep(2);

    return 0;
}

// END functions.cpp