// isabot.cpp
// Autor: Matěj Kudera (xkuder04)
// ISA - Discord Bot

#include <iostream>
#include <string>
#include <regex>
#include <sstream>
#include <algorithm>
#include <map>
#include <list>

#include <sys/types.h>
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "functions.h"
#include "isabot.h"

//////////////////// Hlavní funkce /////////////////////////
int main(int argc, char *argv[])
{
    // Text nápovědy
    std::string help = "Echo bot pro discord. Bot se pomocí obdrženého bot_acces_tokenu připojí na kanál isa-bot v některé z dopstupných guild.\n\
    Na tomto kanálu pak bot odpovýdá echo zprávamy ve formátu: echo: <username> - <message> na veškeré nové zprávy uživatelů.\n\
    \n\
    Spuštění programu:\n\
    ./isabot [-h|--help] [-v|--verbose] -t <bot_access_token>\n\
    \n\
    kde:\n\
    -h|--help : Vypíše nápovědu na standardní výstup.\n\
    -v|--verbose : Bude zobrazovat zprávy, na které bot reaguje na standardní výstup ve formátu <channel> - <username>: <message>.\n\
    -t <bot_access_token> : Zde je nutno zadat autentizační token pro přístup bota na Discord.\n\
    Spuštění programu bez parametrů zobrazí nápovědu.\
    ";

    // Texty k návratových chybám
    std::map<int, std::string> exit_strings;
    exit_strings[3] = "Nepodařilo se vytvořit SSL certifikát.";
    exit_strings[4] = "Nepodařilo se získat ip adresu serveru.";
    exit_strings[5] = "Nepodařilo se vytvořit soket.";
    exit_strings[6] = "Nepodařilo se navázat spojení se serverem.";
    exit_strings[7] = "Nepodařilo se vytvořit SSL spojení se serverem.";
    exit_strings[8] = "Nepodařilo se poslat data přes SSL spojení.";
    exit_strings[9] = "Neočekávaná odpověď serveru discord.com, byla očekávána odpověď ve specifickém tvaru.";
    exit_strings[10] = "Bot se nenachází v žádné quildě.";
    exit_strings[11] = "V guildě neexistuje kanál se jménem isa-bot.";

    bool verbose = false;
    std::string bot_access_token = "";  //NzYwNzc4NDc3MTQ5MzU2MDMy.X3RAKw.GpOwZjIyi_fcJqG9drX9hwy2OaE
    int return_code;

    int sock_fd = 0;
    SSL *ssl_connection;
    SSL_CTX *ssl_certificate;

    ///////////////// Kontrola argumentů programu ////////////////////////////
    if ((return_code = check_args(&verbose, &bot_access_token, argc, argv)) != 3)
    {
        std::cerr << help << std::endl;
        exit(return_code);
    }
    
    /////////////////////////////// Kontrola parametrů bota //////////////////////
    // Pokud je bot ve více guildách postupně je prochází a snaží se najít kanál isa-bot. -> když žádný nenajde konec programu
    std::string guild_id = "";
    std::string isa_channel_id = "";
    std::string last_message_id = "";

    // Zjištění v kterých guildách je bot
    std::string get_bot_guilds = "GET /api/users/@me/guilds HTTP/1.1\r\nHost: discord.com\r\nAuthorization: Bot ";
    get_bot_guilds.append(bot_access_token);
    get_bot_guilds.append("\r\n\r\n");

    std::string json_guild_string = "";
    if ((return_code = create_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate)) != 0)
    {
        std::cerr << exit_strings[return_code] << std::endl;
        exit(return_code);
    }
    
    if ((return_code = get_json_reply(ssl_connection, &json_guild_string, get_bot_guilds)) != 0)
    {
        close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);
        std::cerr << exit_strings[return_code] << std::endl;
        exit(return_code);
    }

    close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);

    // Projidí všech guild ve kterých je bot a nalezení kanálu isa-bot
    // Pracovat se bude s prvním nalezeným kanálem
    std::smatch item_found;
    while(regex_search(json_guild_string, item_found, std::regex("\"id\": \"[0-9]*\"")))
    {
        guild_id = item_found.str();

        // Odmazání nalezené hodnoty aby šlo zjistit jestli je v textu další
        int pos = json_guild_string.find(guild_id);
        json_guild_string = json_guild_string.erase(pos, guild_id.length());

        // Vystříhání id ze stringu
        guild_id.erase(0, 7);
        guild_id.erase(guild_id.length() - 1, guild_id.length());

        // Zjištění jestli v guildě je kanál s názvem isa-bot
        std::string get_guild_channels = "GET /api/guilds/";
        get_guild_channels.append(guild_id);
        get_guild_channels.append("/channels HTTP/1.1\r\nHost: discord.com\r\nAuthorization: Bot ");
        get_guild_channels.append(bot_access_token);
        get_guild_channels.append("\r\n\r\n");

        std::string json_channels_string= "";
        if ((return_code = create_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate)) != 0)
        {
            std::cerr << exit_strings[return_code] << std::endl;
            exit(return_code);
        }
            
        if ((return_code = get_json_reply(ssl_connection, &json_channels_string, get_guild_channels)) != 0)
        {
            close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);
            std::cerr << exit_strings[return_code] << std::endl;
            exit(return_code);
        }

        close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);

        // Získání id kanálu se jménem isa-bot pokud v guildě existuje
        if (get_channel_information(json_channels_string, &isa_channel_id, &last_message_id) == 0)
        {
            break;
        }
    }
    
    // Bot se nenachází v žádné guildě
    if (guild_id.empty())
    {
        std::cerr << exit_strings[BOT_NOT_IN_GUILD] << std::endl;
        exit(BOT_NOT_IN_GUILD);
    }

    // Kanál isa-bot není v žádné guildě
    if (isa_channel_id.empty())
    {
        std::cerr << exit_strings[CHANNEL_DOES_NOT_EXIST] << std::endl;
        exit(CHANNEL_DOES_NOT_EXIST);
    }


    ////////////////////////////// Nastavení odesílání echo zpráv ///////////////////////////////
    while (1)
    {
        // Získání zpráv z kanálu 
        std::string get_new_messages = "GET /api/channels/";
        get_new_messages.append(isa_channel_id);
        get_new_messages.append("/messages");

        // Vybrání pouze zpráv po poslední zpracované zprávě
        // pokud null tak si zažádáme o všechny zprávy
        if (last_message_id != "null")
        {
            get_new_messages.append("?after=");
            get_new_messages.append(last_message_id);
        }

        get_new_messages.append(" HTTP/1.1\r\nHost: discord.com\r\nAuthorization: Bot ");
        get_new_messages.append(bot_access_token);
        get_new_messages.append("\r\n\r\n");

        std::string json_messages = "";
        if ((return_code = create_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate)) != 0)
        {
            std::cerr << exit_strings[return_code] << std::endl;
            exit(return_code);
        }
        
        if ((return_code = get_json_reply(ssl_connection, &json_messages, get_new_messages)) != 0)
        {
            close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);
            std::cerr << exit_strings[return_code] << std::endl;
            exit(return_code);
        }

        close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);

        // Pokud žádné nové zprávy nejsou, tak není třeba nic řešit
        if (json_messages == "[]")
        {
            continue;
        }

        // Rozdělení jednotlivých zpráv podle oddělovače
        std::list <std::string> messages = devide_messages(json_messages, "}, {");

        // Projití všech načtených zpráv a rozhodnutí jestli se má na některou odpovědět a nebo ne
        while (!messages.empty())
        {
            std::string username;
            std::string content;
            std::string message_id;
            std::string recieved_message = messages.back();
            messages.pop_back();

            // Vytáhnutí podstatných informací ze zprávy
            if (get_message_information(recieved_message, &content, &username, &message_id) == 1)
            {
                // Neočekávaná odpověď serveru
                std::cerr << exit_strings[UNEXPECTED_SERVER_REPLY] << std::endl;
                exit(UNEXPECTED_SERVER_REPLY);
            }

            // Nastavení id nejnovější zpracované zprávy
            if (last_message_id == "null")
            {
                last_message_id = message_id;
            }
            else if (last_message_id < message_id)
            {
                last_message_id = message_id;
            }
            else
            {
                continue;
            }
            
            // Odpovězení na zprávu pokud není od bota
            // TODO mázev bot velký i malí
            // echo: <username> - <message>
            if ((username.find("bot") != username.npos) || (username.find("BOT") != username.npos))
            {
                continue;
            }

            ////// Poslání echo zprávy /////
            // Vytvoření Json těla
            std::string json_message = ("{ \"content\": \"echo: ");
            json_message.append(username);
            json_message.append(" - ");
            json_message.append(content);
            json_message.append("\", \"tts\": false}");

            // Vytvoření dotazu
            std::string send_message = "POST /api/channels/";
            send_message.append(isa_channel_id);
            send_message.append("/messages HTTP/1.1\r\nContent-Length: ");
            send_message.append(std::to_string(json_message.length()));
            send_message.append("\r\nContent-Type: application/json\r\nHost: discord.com\r\nAuthorization: Bot ");
            send_message.append(bot_access_token);
            send_message.append("\r\n\r\n");
            send_message.append(json_message);
            send_message.append("\r\n");

            // Odeslani
            std::string json_return_string;
            if ((return_code = create_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate)) != 0)
            {
                std::cerr << exit_strings[return_code] << std::endl;
                exit(return_code);
            }
            
            if ((return_code = get_json_reply(ssl_connection, &json_return_string, send_message)) != 0)
            {
                close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);
                std::cerr << exit_strings[return_code] << std::endl;
                exit(return_code);
            }

            close_ssl_connection(&sock_fd, &ssl_connection, &ssl_certificate);

            // Kontrola jestli byla zpráva úspěšně poslána
            if(!regex_search(json_return_string, item_found, std::regex("\"username\": \".*\"")))
            {
                // Neočekávaná odpověď serveru
                std::cerr << exit_strings[UNEXPECTED_SERVER_REPLY] << std::endl;
                exit(UNEXPECTED_SERVER_REPLY);
            }
            
            // Vypsání zprávy do terminálu pokud je zapnuté verbose
            //<channel> - <username>: <message>
            if (verbose)
            {
                std::cout << "isa-bot - " << username << ": " << content << std::endl;
            }
        }
    }

    return ALL_OK; 
}

// END isabot.cpp