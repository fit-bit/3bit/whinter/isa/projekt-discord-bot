// tests.cpp
// Autor: Matěj Kudera (xkuder04)
// Program pro otestování funkcí z programu bota

#include "functions.h"
#include "isabot.h"

int main()
{
    std::string passed = "\033[1;32mPASSED\033[0m";
    std::string failed = "\033[1;31mFAILED\033[0m";

    // Testy na check_args
    std::cout << "Testy check_args" << std::endl;
    bool verbose;
    std::string bot_access_token;
    int argc;
    char var_0[] =  "./isabot";
    char var_1[] =  "-h";
    char var_2[] =  "-t";
    char var_3[] =  "-v";
    char var_4[] = "dfasd";
    char var_5[] = "fdsafadsfa";

    int result;

    // Vrácení help
    argc = 2;
    char *ptr_array_1[] = {var_0, var_1}; 
    result = check_args(&verbose, &bot_access_token, argc, ptr_array_1);

    if (result == 0)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    // 
    argc = 3;
    char *ptr_array_2[] = {var_0, var_4, var_1}; 
    result = check_args(&verbose, &bot_access_token, argc, ptr_array_2);

    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    argc = 1;
    char *ptr_array_3[] = {var_0}; 
    result = check_args(&verbose, &bot_access_token, argc, ptr_array_3);

    if (result == 0)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    argc = 2;
    char *ptr_array_4[] = {var_0, var_2}; 
    result = check_args(&verbose, &bot_access_token, argc, ptr_array_4);

    if (result == 2)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    argc = 3;
    char *ptr_array_5[] = {var_0, var_2 , var_5}; 
    result = check_args(&verbose, &bot_access_token, argc, ptr_array_5);

    if (result == 3)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    argc = 4;
    char *ptr_array_6[] = {var_0, var_2 , var_5, var_3}; 
    result = check_args(&verbose, &bot_access_token, argc, ptr_array_6);

    if ((result == 3) && (verbose == true) && (bot_access_token == "fdsafadsfa"))
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    std::cout << "" << std::endl;

    // Testy na remove_message_header
    std::cout << "Testy remove_message_header" << std::endl;
    
    std::string result_2;

    //
    result_2 = remove_message_header("fasd\n \ntest");
    if (result_2 == "test")
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result_2 = remove_message_header("");
    if (result_2 == "")
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result_2 = remove_message_header("fdsafasd sdfasd       fdasdfewrfe 4512312 \nfasdf dfsf ds\n \nfdsfasd dsafsdaf 1255 1125\nfdas/#/");
    if (result_2 == "fdsfasd dsafsdaf 1255 1125fdas/#/")
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    std::cout << "" << std::endl;

    // Testy na stop_message_read
    std::cout << "Testy stop_message_read" << std::endl;

    //
    result = stop_message_read("", "0 sdf    ");
    if (result == 0)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = stop_message_read("", "0");
    if (result == 0)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = stop_message_read("Content-Length: 3\n \n123", "fasdfasd asf d");
    if (result == 0)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = stop_message_read("fasd f sa\nContent-Length: 3\nfsdaf sdfsdaf\n \n123", "fasdfasd asf d");
    if (result == 0)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = stop_message_read("", "");
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = stop_message_read("fasdf a sdf\nfsaf888++fsda", "fadsf sd f\n fsadf dsfdf\n");
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    std::cout << "" << std::endl;

    // Testy na get_channel_information
    std::cout << "Testy get_channel_information" << std::endl;

    std::string isa_channel_id;
    std::string last_message_id;

    //
    result = get_channel_information("", &isa_channel_id, &last_message_id);
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_channel_information("fdsaf das fd fasdxasda1245658 sdafsa5/f++", &isa_channel_id, &last_message_id);
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_channel_information("\"id\": \"760833970627280946\", \"last_mess  age_id\": \"762302526744690709\", \"type\": 0, \"name\": \"isa-bot\"", &isa_channel_id, &last_message_id);
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_channel_information("\"id\": \"760833970627280946\", \"last_message_id\": \"762302526744690709\", \"type\": 0, \"name\": \"isa-bot\"", &isa_channel_id, &last_message_id);
    if ((result == 0) && (isa_channel_id == "760833970627280946") && (last_message_id == "762302526744690709"))
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_channel_information("\"id\": \"760833970627280946\", \"last_message_id\": null, \"type\": 0, \"name\": \"isa-bot\"", &isa_channel_id, &last_message_id);
    if ((result == 0) && (isa_channel_id == "760833970627280946") && (last_message_id == "null"))
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    std::cout << "" << std::endl;

    // Testy na devide_messages
    std::cout << "Testy devide_messages" << std::endl;

    std::list <std::string> result_3;
    std::list <std::string> compare;

    //
    result_3 = devide_messages("1-2-3", "-");
    compare = {"1", "2", "3"};
    if (result_3 == compare)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result_3 = devide_messages("1-2-", "-");
    compare = {"1", "2"};
    if (result_3 == compare)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result_3 = devide_messages("a<->b<->c", "<->");
    compare = {"a", "b", "c"};
    if (result_3 == compare)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result_3 = devide_messages("a<->b<->c", "");
    if (result_3.empty())
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result_3 = devide_messages("a", "a");
    if (result_3.empty())
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result_3 = devide_messages("a", "afadsfa");
    if (result_3.empty())
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    std::cout << "" << std::endl;

    // Testy na get_message_information
    std::cout << "Testy get_message_information" << std::endl;

    std::string content;
    std::string username;
    std::string message_id;

    //
    result = get_message_information("", &content, &username, &message_id);
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_message_information("asdfasdf fsdafasd645545--fasd fasd", &content, &username, &message_id);
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_message_information("[{\"id\": \"770352149418213436\", \"type\": 0, \"channel_id\": \"770308723230638090\", \"author\": {\"id\": \"760778477149356032\", \"username\": \"Knight_of_Maids\", \"avatar\": \"3d8923e513084b82de61f5b5586da4ef\", \"discriminator\": \"8431\"}]", &content, &username, &message_id);
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_message_information("[{\"id\": \"770352149418213436\", \"type\": 0, \"chan'el_id\": \"770308723230638090\", \"author\": {\"id\": \"760778477149356032\", \"username\": \"Knight_of_Maids\", \"avatar\": \"3d8923e513084b82de61f5b5586da4ef\", \"discriminator\": \"8431\"}]", &content, &username, &message_id);
    if (result == 1)
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }

    //
    result = get_message_information("[{\"id\": \"770352149418213436\", \"type\": 0, \"content\": \"davln\u011bje\", \"channel_id\": \"770308723230638090\", \"author\": {\"id\": \"760778477149356032\", \"username\": \"Knight_of_Maids\", \"avatar\": \"3d8923e513084b82de61f5b5586da4ef\", \"discriminator\": \"8431\"}]", &content, &username, &message_id);
    if ((result == 0) && (content == "davln\u011bje") && (username == "Knight_of_Maids") && (message_id == "770352149418213436"))
    {
        std::cout << passed << std::endl;
    }
    else
    {
        std::cout << failed << std::endl;
    }


    return 0;
}

// END tests.cpp