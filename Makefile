# Makefile
# Autor: Matěj Kudera (xkuder04)
# Makefile pro složení projektu

C++ = g++
FLAGS_C++ = -std=c++11 -pedantic -Wall

### Překlad ###

all: isabot tests

isabot: isabot.o functions.o
	$(C++) $(FLAGS_C++) isabot.o functions.o -o isabot -lssl -lcrypto

isabot.o: isabot.cpp functions.h isabot.h
	$(C++) $(FLAGS_C++) -c isabot.cpp -o isabot.o -lssl -lcrypto

tests: tests.o functions.o
	$(C++) $(FLAGS_C++) tests.o functions.o -o tests -lssl -lcrypto

tests.o: tests.cpp  isabot.h functions.h
	$(C++) $(FLAGS_C++) -c tests.cpp -o tests.o -lssl -lcrypto

functions.o: functions.cpp isabot.h functions.h
	$(C++) $(FLAGS_C++) -c functions.cpp -o functions.o -lssl -lcrypto

### Pomocné funkce ###

# Spuštění testů programu
test:
	./tests

# Odstranění souborů vytvořených překladem
clean:
	-rm -f isabot tests *.o xkuder04.tar

# Vytvoření odevzdávacího archivu
tar: 
	tar -cvf xkuder04.tar *.cpp *.h Makefile README manual.pdf

# end Makefile
